import os
import re
import sys
import sqlite3
import hashlib
from flask import Flask, request, session, g, redirect, url_for, \
	 abort, render_template, flash
from contextlib import closing

# @todo add column `meta' in `exercises'
# @todo show unsolved exercises
# @todo show random exercise
# @todo edit exercise
# @todo preview solution adding
# @todo optimise SQL queries
# @todo SQL export (database dump)
# @todo LaTeX export

"""
	The Pygments reStructuredText directive
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	This fragment is a Docutils_ 0.4 directive that renders source code
	(to HTML only, currently) via Pygments.

	To use it, adjust the options below and copy the code into a module
	that you import on initialization.  The code then automatically
	registers a ``code-block`` directive that you can use instead of
	normal code blocks like this::

		.. code-block:: python

			My code goes here.

	If you want to have different code styles, e.g. one with line numbers
	and one without, add formatters with their names in the VARIANTS dict
	below.  You can invoke them instead of the DEFAULT one by using a
	directive option::

		.. code-block:: python
			:linenos:

			My code goes here.

	Look at the `directive documentation`_ to get all the gory details.

	.. _Docutils: http://docutils.sf.net/
	.. _directive documentation:
	   http://docutils.sourceforge.net/docs/howto/rst-directives.html

	:copyright: 2007 by Georg Brandl.
	:license: BSD, see LICENSE for more details.
"""

from docutils import core
from docutils import nodes
from docutils.parsers.rst import directives

# Set to True if you want inline CSS styles instead of classes.
INLINESTYLES = False

import pygments
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import get_lexer_by_name, TextLexer

# The default formatter.
DEFAULT = HtmlFormatter(noclasses=INLINESTYLES)

# Add name -> formatter pairs for every variant you want to use.
VARIANTS = {
	'linenos': HtmlFormatter(noclasses=INLINESTYLES, linenos=True),
}

def pygments_directive(name, arguments, options, content, lineno,
					   content_offset, block_text, state, state_machine):
	try:
		lexer = get_lexer_by_name(arguments[0])
	except ValueError:
		# no lexer found - use the text one instead of an exception
		lexer = TextLexer()
	# take an arbitrary option if more than one is given
	formatter = options and VARIANTS[options.keys()[0]] or DEFAULT
	parsed = highlight(u'\n'.join(content), lexer, formatter)
	parsed = '<div class="codeblock">%s</div>' % parsed
	return [nodes.raw('', parsed, format='html')]

pygments_directive.arguments = (1, 0, 1)
pygments_directive.content = 1
pygments_directive.options = dict([(key, directives.flag) for key in VARIANTS])

directives.register_directive('code-block', pygments_directive)

DATABASE = 'data.db'
DEBUG = True
SECRET_KEY = 'development key'

app = Flask(__name__)
app.config.from_object(__name__)

def connect_db():
	if not os.path.exists(app.config['DATABASE']):
		init_db()
	return sqlite3.connect(app.config['DATABASE'])

def init_db():
	print "Creating empty database..."
	with closing(sqlite3.connect(app.config['DATABASE'])) as db:
		with app.open_resource('schema.sql') as f:
			db.cursor().executescript(f.read())
		db.commit()

@app.before_request
def before_request():
	"""Make sure we are connected to the database each request."""
	g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
	"""Closes the database again at the end of the request."""
	if hasattr(g, 'db'):
		g.db.close()

@app.route('/')
def index():
	# @todo show number of exercises lacking solution
	cur  = g.db.execute(
		'select id, exercise from exercises where parentId = -1 order by id desc limit 10')
	rows = [dict(id=row[0], exercise=formatText(row[1])) for row in cur.fetchall()]

	return render_template('index.html', exercises=rows)

def _sources(error = None):
	cur  = g.db.execute('select id, text from sources order by text')
	rows = [dict(id=row[0], text=row[1], exercises=countExercisesBySource(row[0])) \
			for row in cur.fetchall()]

	return render_template('sources.html',
		sources=rows,
		error=error)

@app.route('/sources')
def sources():
	return _sources()

@app.route('/source/add', methods=['POST'])
def addSource():
	if not session.get('loggedIn'):
		abort(401)

	text = request.form['text'].strip()

	if len(text) == 0:
		return _sources("The source cannot be empty")

	g.db.execute('insert into sources (text) values (?)', [text])
	g.db.commit()

	return sources()

def countExercisesByCategory(id):
	cur = g.db.execute(
		'select count(1) from exercises where parentId = -1 and catId = ?', [id])
	return cur.fetchone()[0]

def countExercisesBySource(id):
	cur = g.db.execute(
		'select count(1) from exercises where parentId = -1 and sourceId = ?', [id])
	return cur.fetchone()[0]

def getUsername(id):
	cur = g.db.execute('select username from users where id = ?', [id])
	row = cur.fetchone()
	if row == None:
		return ""
	return row[0]

def hasSubTasks(id):
	cur = g.db.execute(
		'select count(1) from exercises where parentId = ?', [id])
	return (cur.fetchone()[0] > 0)

def wasSolved(id):
	# @todo Improve heuristic.
	if hasSubTasks(id):
		return True

	cur = g.db.execute(
		'select count(1) from solutions where exerciseId = ?', [id])
	return (cur.fetchone()[0] > 0)

def _category(id, request, error=None):
	cur = g.db.execute(
		'select id, userId, exercise from exercises where parentId = -1 and catId = ?', [id])
	items = [dict(id=row[0], exercise=formatText(row[2]), user=getUsername(row[1]), solved=wasSolved(row[0])) for row in cur.fetchall()]

	category = getCategory(id)
	newCat   = category

	if request.method == 'POST':
		newCat = request.form['text']

	return render_template('exercises.html',
		exercises = items,
		catId     = id,
		category  = category,
		newCat    = newCat,
		error     = error)

@app.route('/category/<int:id>')
def category(id):
	return _category(id, request)

@app.route('/source/<int:id>')
def source(id):
	cur  = g.db.execute(
		'select id, userId, exercise from exercises where parentId = -1 and sourceId = ?', [id])
	rows = [dict(id=row[0], exercise=formatText(row[2]), user=getUsername(row[1]), solved=wasSolved(row[0]))
			for row in cur.fetchall()]

	return render_template('exercises.html',
		exercises = rows,
		catId     = None,
		sourceId  = id)

def _categories(error = None):
	cur = g.db.execute('select id, text from categories order by text')
	items = [dict(id=row[0], text=row[1], exercises=countExercisesByCategory(row[0])) \
			for row in cur.fetchall()]

	return render_template('categories.html',
		categories=items,
		error=error)

@app.route('/categories')
def categories():
	return _categories()

@app.route('/category/add', methods=['POST'])
def addCategory():
	if not session.get('loggedIn'):
		abort(401)

	text = request.form['text']

	if len(text.strip()) == 0:
		return _categories("The category name cannot be empty")

	g.db.execute('insert into categories (text) values (?)', [request.form['text']])
	g.db.commit()

	return _categories()

@app.route('/category/rename', methods=['POST'])
def renameCategory():
	if not session.get('loggedIn'):
		abort(401)

	catId = request.form['catId']
	text  = request.form['text'].strip()

	if not existsCategory(catId):
		return _categories("Category not found")

	if not session.get('isAdmin'):
		return _category(catId, request, "Insufficient permissions")

	if len(text) == 0:
		return _category(catId, request, "The category name cannot be empty")

	g.db.execute(
		'update categories set text = ? where id = ?',
		[text, catId])
	g.db.commit()

	return redirect(url_for('category', id=catId))

def getCategories():
	cur = g.db.execute('select id, text from categories order by text')
	return [dict(id=row[0], text=row[1]) for row in cur.fetchall()]

def getSources():
	cur = g.db.execute('select id, text from sources order by text')
	return [dict(id=row[0], text=row[1]) for row in cur.fetchall()]

def getCategory(id):
	cur = g.db.execute('select text from categories where id = ?', [id])
	row = cur.fetchone()
	if row == None:
		return ""
	return row[0]

def getSource(id):
	cur = g.db.execute('select text from sources where id = ?', [id])
	row = cur.fetchone()
	if row == None:
		return ""
	return row[0]

def getExercise(id):
	cur = g.db.execute('select exercise from exercises where id = ?', [id])
	row = cur.fetchone()
	if row == None:
		return ""
	return row[0]

def existsCategory(id):
	cur = g.db.execute('select id from categories where id = ?', [id])
	row = cur.fetchone()
	return row != None

def existsSource(id):
	cur = g.db.execute('select id from sources where id = ?', [id])
	row = cur.fetchone()
	return row != None

def existsExercise(id):
	cur = g.db.execute('select id from exercises where id = ?', [id])
	row = cur.fetchone()
	return row != None

dollar = re.compile(r"[$](.*)[$]")

def formatText(s):
	try:
		# @todo Find a better solution.
		source = ".. role:: latex(raw)\n :format: html\n\n" + s
		source = dollar.sub(r":latex:`$\1$`", source)
		source = source.strip()

		parts = core.publish_parts(source = source, writer_name='html')
		return parts['body_pre_docinfo'] + parts['fragment']
	except:
		return "Error: The text couldn't be rendered: %s" % str(sys.exc_info()[1])

@app.route('/exercise/<int:id>')
def exercise(id):
	cur = g.db.execute(
		'select parentId, catId, sourceId, source, exercise, userId from exercises where id = ?', [id])
	exercise = cur.fetchone()

	if exercise == None:
		abort(404)

	parentId = exercise[0]
	catId    = exercise[1]
	sourceId = exercise[2]
	source   = exercise[3]
	text     = exercise[4]
	userId   = exercise[5]

	cur = g.db.execute(
		'select id, exercise from exercises where parentId = ?', [id])

	subTasks = [dict(id=row[0], exercise=formatText(row[1])) for row in cur.fetchall()]

	cur = g.db.execute(
		'select id, text, userId from solutions where exerciseId = ?', [id])

	solutions = [dict(id=row[0], text=formatText(row[1]), user=getUsername(row[2])) \
				for row in cur.fetchall()]

	return render_template('viewExercise.html',
		id         = id,
		catId      = catId,
		parentId   = parentId,
		category   = getCategory(catId),
		source     = getSource(sourceId),
		sourceText = source,
		exercise   = formatText(text),
		subTasks   = subTasks,
		username   = getUsername(userId),
		solutions  = solutions)

def _previewExercise(request, id, parentId, catId, sourceId, source, exercise):
	return render_template('previewExercise.html',
		id          = id,
		catId       = catId,
		parentId    = parentId,
		sourceId    = sourceId,
		category    = getCategory(catId),
		source      = getSource(sourceId),
		sourceText  = source,
		exercise    = formatText(exercise),
		rawExercise = exercise,
		edit        = True)

def _editExercise(request, id, parentId, catId, sourceId, source, exercise, error = None):
	return render_template('editExercise.html',
		id         = id,
		error      = error,
		catId      = catId,
		parentId   = parentId,
		sourceId   = sourceId,
		source     = source,
		exercise   = exercise,
		categories = getCategories(),
		sources    = getSources())

@app.route('/exercise/edit', methods=['GET', 'POST'])
@app.route('/exercise/edit/<int:id>', methods=['GET', 'POST'])
def editExercise(id = -1):
	if not session.get('loggedIn'):
		abort(401)

	if request.method == 'POST':
		id = request.form['id']

	cur = g.db.execute(
		'select parentId, catId, sourceId, source, exercise, userId from exercises where id = ?', [id])
	exercise = cur.fetchone()

	if exercise == None:
		abort(404)

	parentId = exercise[0]
	catId    = exercise[1]
	sourceId = exercise[2]
	source   = exercise[3]
	text     = exercise[4]
	userId   = exercise[5]

	if userId != session.get('id') and not session.get('isAdmin'):
		return _editExercise(
			request, id, parentId, catId, sourceId, source, text,
			error = "Insufficient permissions")

	if request.method == 'POST':
		catId    = int(request.form['catId'])
		sourceId = int(request.form['sourceId'])
		source   = request.form['source']
		text     = request.form['exercise']

	if 'preview' in request.form:
		return _previewExercise(
			request, id, parentId, catId, sourceId, source, text)

	if 'edit' in request.form:
		# @todo validate input (cf. addExercise())

		g.db.execute(
			'update exercises set catId = ?, sourceId = ?, source = ?, exercise = ? where id = ?',
			[catId, sourceId, source, text, id])
		g.db.commit()

		flash('Exercise edited')

		if parentId == -1:
			return redirect(url_for('exercise', id = id))
		else:
			return redirect(url_for('exercise', id = parentId))

	return _editExercise(request, id, parentId, catId, sourceId, source, text)

def _addExercise(request, parentId = -1, catId = -1, sourceId = -1, error = None):
	exercise = ""
	source   = ""
	parent   = ""

	if request.method == 'POST':
		parentId = int(request.form['parentId'])

	if parentId != -1:
		cur = g.db.execute(
			'select catId, sourceId, source, exercise from exercises where id = ?',
			[parentId])
		row = cur.fetchone()

		if row != None:
			catId    = row[0]
			sourceId = row[1]
			source   = row[2]
			parent   = row[3]

	if request.method == 'POST':
		catId    = int(request.form['catId'])
		source   = request.form['source']
		sourceId = int(request.form['sourceId'])
		exercise = request.form['exercise']

	return render_template('exercise.html',
		error      = error,
		parent     = parent,
		exercise   = exercise,
		catId      = catId,
		parentId   = parentId,
		sourceId   = sourceId,
		source     = source,
		categories = getCategories(),
		sources    = getSources())

@app.route('/exercise/add', methods=['GET', 'POST'])
@app.route('/exercise/add/<int:parentId>', methods=['GET', 'POST'])
@app.route('/exercise/add/cat/<int:catId>', methods=['GET', 'POST'])
@app.route('/exercise/add/src/<int:sourceId>', methods=['GET', 'POST'])
def addExercise(parentId = -1, catId = -1, sourceId = -1):
	if not session.get('loggedIn'):
		abort(401)

	if request.method == 'GET':
		return _addExercise(request, parentId, catId, sourceId)

	catId    = int(request.form['catId'])
	source   = request.form['source']
	sourceId = int(request.form['sourceId'])
	parentId = int(request.form['parentId'])
	exercise = request.form['exercise']

	# @todo validate parent ID

	if 'preview' in request.form:
		return render_template('previewExercise.html',
			category   = getCategory(catId),
			source     = getSource(sourceId),
			sourceText = source,
			exercise   = formatText(exercise),
			catId      = catId,
			sourceId   = sourceId,
			parentId   = parentId,
			edit       = False)

	if 'insert' in request.form:
		if not existsCategory(catId):
			return _addExercise(request, parentId, catId, sourceId, "Invalid category")

		if not existsSource(sourceId):
			return _addExercise(request, parentId, catId, sourceId, "Invalid source")

		if len(source) == 0:
			return _addExercise(request, parentId, catId, sourceId, "No source information provided")

		if len(exercise) == 0:
			return _addExercise(request, parentId, catId, sourceId, "Exercise cannot be empty")

		if parentId != -1:
			cur = g.db.execute('select count(1) from exercises where id = ?', [parentId])
			if cur.fetchone()[0] == 0:
				return _addExercise(request, parentId, catId, sourceId, "Invalid exercise ID")

		g.db.execute(
			'insert into exercises (parentId, userId, catId, sourceId, source, exercise) values (?, ?, ?, ?, ?, ?)',
			[parentId, session.get('id'), catId, sourceId, source, exercise])
		g.db.commit()

		flash('Exercise added')

		if parentId == -1:
			return redirect(url_for('category', id = request.form['catId']))
		else:
			return redirect(url_for('exercise', id = parentId))

	return _addExercise(request, parentId, catId, sourceId)

def _addSolution(request, exerciseId, error):
	sourceId = -1
	solution = ""

	if request.method == 'POST':
		solution = request.form['solution']

	return render_template('solution.html',
		error=error,
		exerciseId=exerciseId,
		exercise=getExercise(exerciseId),
		solution=solution)

def _editSolution(request, id, exercise="", solution="", error=None):
	if request.method == 'POST':
		solution = request.form['solution']

	return render_template('editSolution.html',
		error    = error,
		id       = id,
		exercise = exercise,
		solution = solution)

@app.route('/solution/edit/<int:id>', methods=['GET', 'POST'])
def editSolution(id):
	if not session.get('loggedIn'):
		abort(401)

	cur = g.db.execute('select text, exerciseId, userId from solutions where id = ?', [id])
	solution = cur.fetchone()

	text       = solution[0]
	exerciseId = solution[1]
	userId     = solution[2]

	if solution == None:
		return _editSolution(request, id, error="Solution not found")

	if userId != session.get('id') and not session.get('isAdmin'):
		return _editSolution(request, id, error="Insufficient permissions")

	if 'edit' in request.form:
		g.db.execute(
			'update solutions set text = ? where id = ?',
			[request.form['solution'], id])
		g.db.commit()

		flash('Solution edited')

		return redirect(url_for('exercise', id=str(exerciseId)))

	cur = g.db.execute('select exercise from exercises where id = ?', [exerciseId])
	exercise = cur.fetchone()

	return _editSolution(request, id, exercise=exercise[0], solution=text)

@app.route('/solution/add/<int:exerciseId>', methods=['GET', 'POST'])
def addSolution(exerciseId):
	if not session.get('loggedIn'):
		abort(401)

	if request.method == 'GET':
		return _addSolution(request, exerciseId, None)

	if not existsExercise(exerciseId):
		return _addSolution(request, exerciseId, "Invalid exercise")

	solution = request.form['solution'].strip()

	if len(solution) == 0:
		return _addSolution(request, exerciseId, "The solution cannot be empty")

	g.db.execute(
		'insert into solutions (userId, exerciseId, text) values (?, ?, ?)',
		[session.get('id'), exerciseId, solution])
	g.db.commit()

	flash('Solution added')

	return redirect(url_for('exercise', id=str(exerciseId)))

def _register(request, error):
	return render_template('register.html', error=error,
		username = request.form['username'],
		email    = request.form['email'])

@app.route('/register', methods=['GET', 'POST'])
def register():
	if request.method == 'GET':
		return render_template('register.html', error=None, username="", email="")

	if len(request.form['username']) < 5:
		return _register(request, 'Username must be longer than 4 characters')

	if len(request.form['password']) < 5:
		return _register(request, 'Password must be longer than 4 characters')

	if '@' not in request.form['email'] or '.' not in request.form['email']:
		return _register(request, 'Invalid e-mail address')

	cur = g.db.execute('select count(1) from users where username = ?',
		[request.form['username']])

	if cur.fetchone()[0] != 0:
		return _register(request, 'Username already exists')

	g.db.execute('insert into users (username, password, email, isAdmin) values (?, ?, ?, 0)',
		[request.form['username'], hashlib.sha1(request.form['password']).hexdigest(), request.form['email']])
	g.db.commit()

	flash('The user was created. You can now log in.')
	return redirect(url_for('login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
	error    = None
	username = ""

	if request.method == 'POST':
		username = request.form['username']
		password = request.form['password']

		cur = g.db.execute('select id, isAdmin from users where username = ? and password = ?',
			[username, hashlib.sha1(password).hexdigest()])
		row = cur.fetchone()

		if row == None:
			error = 'Invalid username or password'
		else:
			session['id']       = row[0]
			session['isAdmin']  = row[1]
			session['loggedIn'] = True

			flash('Log in successful')

			return redirect(url_for('index'))

	return render_template('login.html', error=error, username=username)

@app.route('/logout')
def logout():
	session.pop('loggedIn', None)
	flash('You were logged out')
	return redirect(url_for('index'))
