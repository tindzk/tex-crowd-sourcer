TeX crowd sourcer is a web application for working collaboratively on exercises
involving the use mathematical symbols.

It's written in Python 2 and is based upon Flask, Pygments, reStructuredText and
SQLite. MathJax is used for rendering the TeX code.

The project is licensed under the terms of the EUPL v1.1.

--
Tim Nieradzik
timn@uni-bremen.de
