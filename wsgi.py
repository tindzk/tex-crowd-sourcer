#!/usr/bin/python2
import App
from flup.server.fcgi import WSGIServer

if __name__ == '__main__':
	WSGIServer(App.app).run()
