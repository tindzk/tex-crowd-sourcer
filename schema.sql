drop table if exists categories;
create table categories (
	id integer primary key autoincrement,
	text string not null
);

drop table if exists sources;
create table sources (
	id integer primary key autoincrement,
	text string not null
);

drop table if exists users;
create table users (
	id integer primary key autoincrement,
	username string not null,
	password string not null,
	email string not null,
	isAdmin integer
);

drop table if exists exercises;
create table exercises (
	id integer primary key autoincrement,
	parentId integer,
	catId integer,
	sourceId integer,
	source string not null,
	exercise string not null,
	userId integer
);

drop table if exists solutions;
create table solutions (
	id integer primary key autoincrement,
	exerciseId integer,
	text string not null,
	userId integer
);
