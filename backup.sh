#!/bin/sh
mkdir backup &> /dev/null
cd backup
git init
sqlite3 ../data.db .dump > backup.sql
git add backup.sql
git commit -a -m update
